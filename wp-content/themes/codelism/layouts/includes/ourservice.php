<section id="our-service">
  <div id="web-design" class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-xs-12">
          <img src="<?php bloginfo('template_directory'); ?>/images/responsive.png"/>
        </div>
        <div class="col-sm-6 col-xs-12">
          <h1 class="content-title danger">
            ไม่ว่าความต้องการเว็ปไซต์ของคุณจะเป็นแบบใด เราสามารถจัดทำให้ได้ตามทุกความต้องการ
          </h1>
          <p>
            ทุกการออกแบบและพัฒนา เราให้ความสำคัญทุกความต้องการของคุณ ไม่ว่าคุณต้องการเว็ปไซต์องค์กร หรือเว็ปไซต์สำหรับขายสินค้า เราเข้าถึง ทำความเข้าใจ เพื่อให้ได้เว็ปไซต์ที่ตรงใจคุณมากที่สุด
            อีกทั้งเรายังสามมารถ ออกแบบโลโก้ ไอค่อน หรือรูปภาพประกอบ 
          </p>
          <a href="http://m.me/FeatureStudioTH" target="_blank" class="btn btn-lg btn-danger btn-animate">
            คุยกับทีมของเรา
          </a>
          <!-- <a href="#contact" class="btn btn-lg btn-primary btn-animate">
            ดูค่าบริการเบื้องต้น
          </a> -->
        </div>
      </div>
    </div>
  </div>
  <div id="graphic-design" class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-xs-12 pull-right">
          <img src="<?php bloginfo('template_directory'); ?>/images/graphic.png"/>
        </div>
        <div class="col-sm-6 col-xs-12">
          <h1 class="content-title info">
            งานออกแบบการฟฟิก เป็นเอกลักษณ์ เข้าใจความต้องการ แก้ไขได้ตามความพอใจ
          </h1>
          <p>
            เราบริการออกแบบโบว์ชัวร์ ป้ายโฆษณา แบบเนอร์เว็ปไซต์ ออกแบบจัดเรียงหนังสือ วาดรูปประกอบ วาดรูปการ์ตูน วาดไอค่อน ออกแบบโลโก้ ออกแบบลายเสื้อ ในราคาที่จับต้องได้
          </p>
          <a href="http://m.me/FeatureStudioTH" target="_blank"  class="btn btn-lg btn-danger btn-animate">
            คุยกับทีมของเรา
          </a>
          <!-- <a href="#contact" class="btn btn-lg btn-primary btn-animate">
            ดูค่าบริการเบื้องต้น
          </a> -->
        </div>
      </div>
    </div>
  </div>
</section>