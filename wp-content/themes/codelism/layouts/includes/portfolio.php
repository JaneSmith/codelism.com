<section id="portfolio" class="content">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <img class="content-title" src="<?php bloginfo('template_directory'); ?>/images/title/Portfolio.png"/>
      </div>
    </div>
    <div class="row">
      <?php
        $portfolio_query = new WP_Query(array( 'category_name' => 'portfolio' ));
        if ( $portfolio_query->have_posts() ) {
          // Start the Loop.
          while ( $portfolio_query->have_posts() ) {
                  $portfolio_query->the_post();
                  ?>

                 <div class="col-sm-4">
                  <?php if ( has_post_thumbnail() ) : ?>
                      <a class="thumbnail flip_container" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <div class="flipper">
                          <div class="thumbnail-img face front">
                            <?php the_post_thumbnail('medium'); ?>
                          </div>
                  <?php endif; ?>
                        <div class="caption face back">
                          <h3><?php echo get_the_title(); ?></h3>
                          <p><?php echo get_the_excerpt(); ?></p>
                        </div>
                      </div>
                    </a>
                  </div>
              <?php
          }
        }
        else{

        }
          // If no content, include the "No posts found" template.
        wp_reset_postdata();
      ?>
    </div>
  </div>
</section>
  
