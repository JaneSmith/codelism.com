<div id="mainnav" class="navbar-fixed-top">
	<nav class="navbar navbar-index">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed btn-rotate" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar top-bar"></span>
					<span class="icon-bar middle-bar"></span>
					<span class="icon-bar bottom-bar"></span>
				</button>
				<a class="navbar-brand hidden" href="#hero-header">
					<img class="brand-logo" src="<?php bloginfo('template_directory'); ?>/images/featureStudio-w-full.png"/>
				</a>
			</div>
			<div id="navbar-collapse" class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="totop active" href="#hero-header">Home</a></li>
					<li><a class="totop" href="#our-service">Our Service</a></li>
					<li><a class="totop" href="#howtowork">How to Work</a></li>
					<li><a class="totop" href="#portfolio">Portfolio</a></li>
					<li><a class="totop" href="#ourteam">Our Team</a></li>
					<li>
						<a class="btn btn-danger btn-animate totop" href="http://m.me/FeatureStudioTH" target="_blank" >Contact Us</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</div>