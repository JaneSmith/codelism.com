<section id="ourteam" class="content">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <img class="content-title" src="<?php bloginfo('template_directory'); ?>/images/title/ourTeam.png"/>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="thumbnail">
          <div class="thumbnail-img">
            <img src="<?php bloginfo('template_directory'); ?>/images/team/execter.png" alt="programer">
          </div>
          <div class="caption">
            <h2>Execter</h2>
            <p>Web Application Programer</p>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="thumbnail ">
          <div class="thumbnail-img">
            <img src="<?php bloginfo('template_directory'); ?>/images/team/ae.png" alt="webdesigner">
          </div>
          <div class="caption">
            <h2>ae</h2>
            <p>Web Designer & Front End Deverloper</p>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="thumbnail">
          <div class="thumbnail-img">
            <img src="<?php bloginfo('template_directory'); ?>/images/team/otaku.png" alt="charector design">
          </div>
          <div class="caption">
            <h2>otaku</h2>
            <p>Charactor Designer</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>