<nav class="navbar">
    <div class="container">
        <div class="navbar-brand">
            <img src="<?php echo get_bloginfo('template_url') ?>/assets/images/logo/codelism.png" alt="<?php bloginfo( 'name' ); ?>Logo">
        </div>

        <ul class="nav navbar-right">
            <li>
                <a href="#" class="btn btn-round btn-success">ติดต่อเรา</a>
            </li>
        </ul>
    </div>
</nav>