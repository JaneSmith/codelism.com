<?php include('header.php'); ?>
<div id="single-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="content-warpper">
					<div class="content-header">
						<h2>Contact Us</h2>
					</div>
					<div class="content-detail">
						<?php remove_filter('the_content', 'seed_social_auto', 63); ?>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>


<h1>Contact Us</h1>
<p style="font-size: 24px;">ถ้าคุณพร้อมที่จะเริ่มโปรเจคกับเราแล้ว</p>
โปรดบอกรายละเอียดนิดๆ หน่อยๆ เกี่ยวกับโปรเจคของคุณให้เรารู้ เราจะติดต่อกลับคุณให้เร็วที่สุด
<form id="contact-form">
<div class="form-group"><label for="InputName">ชื่อของคุณ</label>
<input id="InputName" class="form-control" type="text" placeholder="ชื่อ - นามสกุล" /></div>
<div class="form-group"><label for="InputEmail">อีเมลล์</label>
<input id="InputEmail" class="form-control" type="email" placeholder="happy@mail.com" /></div>
<div class="form-group"><label for="InputCompany">บริษัทหรือองค์กร</label>
<input id="InputCompany" class="form-control" type="text" placeholder="ชื่อบริษัทหรือองค์กร" /></div>
<div class="form-group"><label for="TextareaDescription">รายละเอียดเกี่ยวกับโปรเจค</label>
<textarea id="TextareaDescription" class="form-control" cols="30" name="TextareaDescription" rows="8"></textarea></div>
<div class="form-group">
<div class="input-file"><label for="InputFile">เอกสารเพิ่มเติม (ถ้ามี)</label>
<input id="InputFile" type="file" /></div>
</div>
<button class="btn btn-info btn-lg btn-animate" type="submit">เริ่มงานกันเลย</button>

</form>
