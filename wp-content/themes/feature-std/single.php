<?php get_header(); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/single.css">

<div id="single-content">
	<?php		        
    while ( have_posts() ) : the_post();
    ?>
    <div class="content-warpper">
    	<div class="content-header">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<h2><?php echo get_the_title(); ?></h2>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="content-shared">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<h4>แชร์บนโซเชียล : <?php if(function_exists('seed_social')) {seed_social();} ?></h4>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="content-detail">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-9 col-sm-12">
    					<?php echo get_the_content(); ?>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="recent-warpper">
    		<div class="container">
	    		<div class="row">
	    			<div class="col-md-12">
						<div class="page-header">
							<h3>
								Related Pages
							</h3>
						</div>
						<?php
							$recent_posts = wp_get_recent_posts(array( 'category_name' => 'portfolio' ));
							foreach( $recent_posts as $recent ){
						?>
							<div class="col-sm-4">
						    	<div class="list-group">							
									<?php if ( has_post_thumbnail() ) : ?>
										<a href="<?php echo get_permalink($recent["ID"]); ?>" class="list-group-item recent-thumbnail">
											<?php echo get_the_post_thumbnail($recent['ID'], 'medium'); ?>
											<span><?php echo $recent["post_title"]; ?></span>
										</a>									
									<?php endif; ?>							
								</div>		    	
						    </div>
					    <?php
							}
						?>
					</div>
	    		</div>
	    	</div>
    	</div>
	    	
    </div>
	<?php
        endwhile;
    ?>					
</div>
<?php include('layouts/includes/contact.php'); ?>
<?php get_footer(); ?>