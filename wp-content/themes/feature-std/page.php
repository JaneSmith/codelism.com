<?php get_header(); ?>
<?php
	if ( is_page( 'contact' ) ) { 
	?>  
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/single.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/contact.css">
	<?php
	}   
?>
<?php
	if ( is_page( 'portfolio' ) ) { 
	?>  
		<section id="portfolio" class="content page warpper">
		  <div class="container">
		    <div class="row">
		      <div class="col-xs-12">
		        <img class="content-title" src="<?php bloginfo('template_directory'); ?>/images/title/Portfolio.png"/>
		      </div>
		    </div>
		    <div class="row">
		      <?php
		        $portfolio_query = new WP_Query(array( 'category_name' => 'portfolio' ));
		        if ( $portfolio_query->have_posts() ) {
		          // Start the Loop.
		          while ( $portfolio_query->have_posts() ) {
		                  $portfolio_query->the_post();
		                  ?>

		                 <div class="col-sm-4">
		                  <?php if ( has_post_thumbnail() ) : ?>
		                      <a class="thumbnail flip_container" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
		                        <div class="flipper">
		                          <div class="thumbnail-img face front">
		                            <?php the_post_thumbnail('medium'); ?>
		                          </div>
		                  <?php endif; ?>
		                        <div class="caption face back">
		                          <h3><?php echo get_the_title(); ?></h3>
		                          <p><?php echo get_the_excerpt(); ?></p>
		                        </div>
		                      </div>
		                    </a>
		                  </div>
		              <?php
		          }
		        }
		        else{

		        }
		          // If no content, include the "No posts found" template.
		        wp_reset_postdata();
		      ?>
		    </div>
		  </div>
		</section>
	<?php
	}else if ( is_page( 'howtowork' ) ){
	?>
		<section id="howtowork" class="content page warpper">
		  <div class="container">
		    <div class="row">
		      <div class="col-xs-12">
		        <img class="content-title" src="<?php bloginfo('template_directory'); ?>/images/title/HowtoWork.png"/>
		      </div>
		    </div>
		    <div class="row">
		      <div class="col-sm-3 thumbnail">
		        <div class="">
		          <img class="thumbnail-icon" src="<?php bloginfo('template_directory'); ?>/images/content/icon-1.png"/>
		          <div class="caption">
		            <h3>1.คุยรายละเอียดงาน</h3>
		            <p>
		              พูดคุยเกี่ยวกับความต้องการของลูกค้า ว่าอยากได้เว็ปไซต์แนวไหน 
		              มีฟังก์ชั่นการทำงานของเว็ปไซต์อะไรบ้าง
		            </p>
		          </div>
		        </div>
		      </div>
		      
		      <div class="col-sm-3 thumbnail">
		        <div class="">
		          <img class="thumbnail-icon" src="<?php bloginfo('template_directory'); ?>/images/content/icon-2.png"/>
		          <div class="caption">
		            <h3>2.ตกลงราคาและระยะเวลา</h3>
		            <p>
		              ทางเราจะเสนอราคาตามความยากง่ายของงาน แล้วหลังจากนั้น ก็จะเริ่มวางแผนการทำงาน
		            </p>
		          </div>
		        </div>
		      </div>

		      <div class="col-sm-3 thumbnail">
		        <div class="">
		          <img class="thumbnail-icon" src="<?php bloginfo('template_directory'); ?>/images/content/icon-3.png"/>
		          <div class="caption">
		            <h3>3.ออกแบบเว็ปไซต์</h3>
		            <p>
		              เราออกแบบเว็ปใซต์ใหม่ เพื่อให้ตรงใจลูกค้า มากที่สุด ลูกค้าสามารถแก้แบบได้ในขั้นตอนนี้
		            </p>
		          </div>
		        </div>
		      </div>

		      <div class="col-sm-3 thumbnail">
		        <div class="">
		          <img class="thumbnail-icon" src="<?php bloginfo('template_directory'); ?>/images/content/icon-4.png"/>
		          <div class="caption">
		            <h3>4.พัฒนาและส่งมอบ</h3>
		            <p>
		              เขียนโค้ดตามแบบที่ตกลงกันไว้ เทสระบบ เช็คความเรียบร้อยของการทำงาน พร้อมส่งมอบ
		            </p>
		          </div>
		        </div>
		      </div>
		    </div>
		  </div>
		</section>
	<?php
	}else if ( is_page( 'ourservice' ) ){
	?>
		<section id="our-service" class="page warpper">
		  <div id="web-design" class="content page">
		    <div class="container">
		      <div class="row">
		        <div class="col-sm-6 col-xs-12">
		          <img src="<?php bloginfo('template_directory'); ?>/images/responsive.png"/>
		        </div>
		        <div class="col-sm-6 col-xs-12">
		          <h1 class="content-title danger">
		            ไม่ว่าความต้องการเว็ปไซต์ของคุณจะเป็นแบบใด เราสามารถจัดทำให้ได้ตามทุกความต้องการ
		          </h1>
		          <p>
		            ทุกการออกแบบและพัฒนา เราให้ความสำคัญทุกความต้องการของคุณ ไม่ว่าคุณต้องการเว็ปไซต์องค์กร หรือเว็ปไซต์สำหรับขายสินค้า เราเข้าถึง ทำความเข้าใจ เพื่อให้ได้เว็ปไซต์ที่ตรงใจคุณมากที่สุด
		            อีกทั้งเรายังสามมารถ ออกแบบโลโก้ ไอค่อน หรือรูปภาพประกอบ 
		          </p>
		          <a href="<?php echo site_url('/contact/', 'html'); ?>" class="btn btn-lg btn-danger btn-animate">
		            ขอใบเสนอราคา
		          </a>
		          
		        </div>
		      </div>
		    </div>
		  </div>
		  <div id="graphic-design" class="content">
		    <div class="container">
		      <div class="row">
		        <div class="col-sm-6 col-xs-12 pull-right">
		          <img src="<?php bloginfo('template_directory'); ?>/images/graphic.png"/>
		        </div>
		        <div class="col-sm-6 col-xs-12">
		          <h1 class="content-title info">
		            งานออกแบบการฟฟิก เป็นเอกลักษณ์ เข้าใจความต้องการ แก้ไขได้ตามความพอใจ
		          </h1>
		          <p>
		            เราบริการออกแบบโบว์ชัวร์ ป้ายโฆษณา แบบเนอร์เว็ปไซต์ ออกแบบจัดเรียงหนังสือ วาดรูปประกอบ วาดรูปการ์ตูน วาดไอค่อน ออกแบบโลโก้ ออกแบบลายเสื้อ ในราคาที่จับต้องได้
		          </p>
		          <a href="<?php echo site_url('/contact/', 'html'); ?>" class="btn btn-lg btn-danger btn-animate">
		            ขอใบเสนอราคา
		          </a>
		          
		        </div>
		      </div>
		    </div>
		  </div>
		</section>
	<?php
	}else if ( is_page( 'ourteam' ) ){
	?>
		<section id="ourteam" class="content page warpper">
		  <div class="container">
		    <div class="row">
		      <div class="col-xs-12">
		        <img class="content-title" src="<?php bloginfo('template_directory'); ?>/images/title/ourTeam.png"/>
		      </div>
		    </div>
		    <div class="row">
		      <div class="col-sm-4">
		        <div class="thumbnail">
		          <div class="thumbnail-img">
		            <img src="<?php bloginfo('template_directory'); ?>/images/team/execter.png" alt="programer">
		          </div>
		          <div class="caption">
		            <h2>Execter</h2>
		            <p>Web Application Programer</p>
		          </div>
		        </div>
		      </div>
		      <div class="col-sm-4">
		        <div class="thumbnail ">
		          <div class="thumbnail-img">
		            <img src="<?php bloginfo('template_directory'); ?>/images/team/ae.png" alt="webdesigner">
		          </div>
		          <div class="caption">
		            <h2>ae</h2>
		            <p>Web Designer & Front End Deverloper</p>
		          </div>
		        </div>
		      </div>
		      <div class="col-sm-4">
		        <div class="thumbnail">
		          <div class="thumbnail-img">
		            <img src="<?php bloginfo('template_directory'); ?>/images/team/otaku.png" alt="charector design">
		          </div>
		          <div class="caption">
		            <h2>otaku</h2>
		            <p>Charactor Designer</p>
		          </div>
		        </div>
		      </div>
		    </div>
		  </div>
		</section>
	<?php
	}else if ( is_page( 'webpackage' ) ){
	?>
		<div id="page-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						
					</div>
				</div>
			</div>
		</div>
	<?php
	}else{
	?>
		<div id="page-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php
							// Start the loop.
							while ( have_posts() ) : the_post();
							// Include the page content template.
						?>
						<div id="post-<?php the_ID(); ?>" class="page-warpper">
							<div class="page-header">
								<h1><?php echo get_the_title(); ?></h1>
							</div>
							<div class="page-detail">
								<?php the_post_thumbnail(); ?>
								<?php
									the_content();

									wp_link_pages( array(
										'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'featurestd' ) . '</span>',
										'after'       => '</div>',
										'link_before' => '<span>',
										'link_after'  => '</span>',
										'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'featurestd' ) . ' </span>%',
										'separator'   => '<span class="screen-reader-text">, </span>',
									) );
								?>
							</div>
							<?php

									// If comments are open or we have at least one comment, load up the comment template.
									if ( comments_open() || get_comments_number() ) {
										comments_template();
									}

									// End of the loop.
								endwhile;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php	
	}   
?>
	

<?php get_footer(); ?>