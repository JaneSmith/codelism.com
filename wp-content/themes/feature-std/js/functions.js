$(document).ready(function(){
	var activeBtn, d, x, y;
	$(".btn-animate").click(function(e){
	    if($(this).find(".activeBtn").length === 0){
	        $(this).prepend("<span class='activeBtn'></span>");
	    }
	         
	    activeBtn = $(this).find(".activeBtn");
	    activeBtn.removeClass("animateBtn");
	     
	    if(!activeBtn.height() && !activeBtn.width()){
	        d = Math.max($(this).outerWidth(), $(this).outerHeight());
	        activeBtn.css({height: d, width: d});
	    }
	     
	    x = e.pageX - $(this).offset().left - activeBtn.width()/2;
	    y = e.pageY - $(this).offset().top - activeBtn.height()/2;
	     
	    activeBtn.css({top: y+'px', left: x+'px'}).addClass("animateBtn");
	});

	$('.btn-rotate').click(function(){
		$(this).closest('#mainnav').toggleClass('bg-collapse');
	}),600;
});