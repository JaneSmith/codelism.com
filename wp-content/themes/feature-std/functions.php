<?php 
	
	function featurestd_script(){
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
		wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
		wp_enqueue_style( 'bootstrap-mod', get_template_directory_uri() . '/css/bootstrap-mod.css');
		wp_enqueue_style( 'material-btn', get_template_directory_uri() . '/css/material-btn.css');
		wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css');

		wp_enqueue_script( 'jquery js', get_template_directory_uri() . '/js/jquery.min.js');
		wp_enqueue_script( 'bootstrap js', get_template_directory_uri() . '/js/bootstrap.min.js');
		wp_enqueue_script( 'functions js', get_template_directory_uri() . '/js/functions.js');
	}
	add_action('wp_enqueue_scripts', 'featurestd_script');

	// Get URL of first image in a post
	function featurestd_first_image() {
		global $post, $posts;
		$first_img = '';
		ob_start();
		ob_end_clean();
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
		$first_img = $matches [1] [0];

		// no image found display default image instead
		if(empty($first_img)){
			$first_img = "/images/default.jpg";
		}
		return $first_img;
	}

	add_theme_support( 'post-thumbnails' ); 
?>
