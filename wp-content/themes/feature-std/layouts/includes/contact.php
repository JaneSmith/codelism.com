<section id="contact" class="content">
	<div class="container">
		
			<div class="row">
				<div class="col-sm-6">
					<div class="contact-group">
						<h2>
							Contact Us
						</h2>
						<ul class="social">
							<li>
								<a href="https://www.facebook.com/FeatureStudioTH/" target="_blank">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-line">Line</i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-youtube"></i>
								</a>
							</li>
							<!-- <li>
								<a href="<?php echo site_url('/contact/', 'html'); ?>">
									<i class="fa fa-envelope-o"></i>
								</a>
							</li> -->
						</ul>
						<!-- <h4>
							<i class="fa fa-envelope-o" aria-hidden="true"></i> aeglamorous@gmail.com
						</h4> -->
						<h4>
							<i class="fa fa-mobile" aria-hidden="true"></i> 
							084 409 4069 (เอ้), 
							088 650 4807 (เล็ก)
						</h4>
						<h4>
							<a href="http://m.me/FeatureStudioTH" target="_blank" class="btn btn-line btn-info btn-animate">
								คุยกับทีมของเรา
							</a>
						</h4>
					</div>					
				</div>
				<div class=" col-sm-6">
					<img src="<?php bloginfo('template_directory'); ?>/images/c1.png" alt="">
				</div>
			</div>
		</div>			
	</div>
</section>