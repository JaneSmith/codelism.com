<div id="mainnav" class="bg allpage">
	<nav class="navbar">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed btn-rotate" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar top-bar"></span>
					<span class="icon-bar middle-bar"></span>
					<span class="icon-bar bottom-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
					<img class="brand-logo" src="<?php bloginfo('template_directory'); ?>/images/featureStudio-w-full.png"/>
				</a>
			</div>
			<div id="navbar-collapse" class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo get_home_url(); ?>">Home</a></li>
					<li><a href="<?php echo site_url('/ourservice/', 'html'); ?>">Our Service</a></li>
					<li><a href="<?php echo site_url('/howtowork/', 'html'); ?>">How to Work</a></li>
					<li><a href="<?php echo site_url('/portfolio/', 'html'); ?>">Portfolio</a></li>
					<li><a href="<?php echo site_url('/ourteam/', 'html'); ?>">Our Team</a></li>
					<li>
						<a class="btn btn-danger btn-animate" href="http://m.me/FeatureStudioTH" target="_blank" ">Contact Us</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</div>