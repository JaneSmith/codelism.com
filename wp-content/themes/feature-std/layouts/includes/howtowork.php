<section id="howtowork" class="content">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <img class="content-title" src="<?php bloginfo('template_directory'); ?>/images/title/HowtoWork.png"/>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3 thumbnail">
        <div class="">
          <img class="thumbnail-icon" src="<?php bloginfo('template_directory'); ?>/images/content/icon-1.png"/>
          <div class="caption">
            <h3>1.คุยรายละเอียดงาน</h3>
            <p>
              พูดคุยเกี่ยวกับความต้องการของลูกค้า ว่าอยากได้เว็ปไซต์แนวไหน 
              มีฟังก์ชั่นการทำงานของเว็ปไซต์อะไรบ้าง
            </p>
          </div>
        </div>
      </div>
      
      <div class="col-sm-3 thumbnail">
        <div class="">
          <img class="thumbnail-icon" src="<?php bloginfo('template_directory'); ?>/images/content/icon-2.png"/>
          <div class="caption">
            <h3>2.ตกลงราคาและระยะเวลา</h3>
            <p>
              ทางเราจะเสนอราคาตามความยากง่ายของงาน แล้วหลังจากนั้น ก็จะเริ่มวางแผนการทำงาน
            </p>
          </div>
        </div>
      </div>

      <div class="col-sm-3 thumbnail">
        <div class="">
          <img class="thumbnail-icon" src="<?php bloginfo('template_directory'); ?>/images/content/icon-3.png"/>
          <div class="caption">
            <h3>3.ออกแบบเว็ปไซต์</h3>
            <p>
              เราออกแบบเว็ปใซต์ใหม่ เพื่อให้ตรงใจลูกค้า มากที่สุด ลูกค้าสามารถแก้แบบได้ในขั้นตอนนี้
            </p>
          </div>
        </div>
      </div>

      <div class="col-sm-3 thumbnail">
        <div class="">
          <img class="thumbnail-icon" src="<?php bloginfo('template_directory'); ?>/images/content/icon-4.png"/>
          <div class="caption">
            <h3>4.พัฒนาและส่งมอบ</h3>
            <p>
              เขียนโค้ดตามแบบที่ตกลงกันไว้ เทสระบบ เช็คความเรียบร้อยของการทำงาน พร้อมส่งมอบ
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>