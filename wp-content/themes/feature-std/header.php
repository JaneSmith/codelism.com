<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <title>codelism</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_directory'); ?>/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_directory'); ?>/images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php bloginfo('template_directory'); ?>/images/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php bloginfo('template_directory'); ?>/images/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">


  <?php wp_head(); ?>
  <style type="text/css">
	html{
		margin-top: 0px !important;
	}
  </style>
  <?php if( is_page( array( 'ourservice', 'howtowork', 'portfolio', 'ourteam', 'contact' ) )){ ?>
  	<style type="text/css">
		.seed-social{
			display: none;
		}
		.page-header{
			display: none;
		}
	</style>
	<script>
  		$(document).ready(function(){
  			var windowHeight = $(window).height();
  			var navHeight = $('#mainnav').height();
  			var footerHeight = $('#footer').height();
  			var pageMinHeight = (windowHeight - navHeight) - (footerHeight + 30);

  			$('.warpper').css({
  				'min-height' : pageMinHeight,
  			});

  			$('input[type=submit]').addClass('btn btn-info btn-animate');
  		});
  	</script>
	<?php
	}
	?>
  <script>
  	
  </script>
</head>
<body>
<?php
if ( is_home() ) {
	?>
    <section id="hero-header" class="container-fluid">
	  <?php include('layouts/includes/mainnav-index.php'); ?>
	  <div class="container">
	    <div class="row">
	      <div class="col-md-12">
	        <div class="hero-header-caption">
	          <img class="logo" src="<?php bloginfo('template_directory'); ?>/images/featureStudio-w-full.png"/>
	          <p class="title">
	            Codelism เป็นทีมนักพัฒนาเว็ปไซต์ ที่มีจุดเริ่มต้นมาจากการทำโปรเจคเว็ปไซต์ <br>
	            แต่ด้วยเคมีที่เข้ากันอย่างประหลาดและประสบการณ์ที่ผ่านร้อนผ่านหนาวมาด้วยกัน <br>
	            จึงได้นำพาเรามารวมเป็น Feature Studio
	          </p>
	          <a href="http://m.me/FeatureStudioTH" target="_blank" class="btn btn-danger btn-round btn-animate totop">
	            คุยกับทีมของเรา
	          </a>
	        </div>  
	      </div>
	    </div>
	  </div>
	</section>
	<?php
} else {
	?>
   <?php include('layouts/includes/mainnav.php'); ?>
<?php
}
?>	
