<?php get_header(); ?>
<style>
  .btn-round{
    padding: 6px 60px;
    border-radius: 65px;
    font-size: 20px !important;
    margin-top: 45px;
  }
</style>
<?php
if ( is_home() ) {
  ?>
<script>
  $(document).ready(function(){
    $(window).scroll(function(){
      var ScrollTop = $(window).scrollTop();
      var ourSection = $('#our-service').offset().top;
      var nextSection = $('#our-service').offset().top - 60;
      var marginCount = nextSection - ScrollTop;
      var paddingCount = nextSection - 250;
          windowWidth = $(window).width();

      if(ScrollTop > paddingCount){
        $('.navbar-brand').removeClass('hidden');
      }else{
         $('.navbar-brand').addClass('hidden');
      }

      if( marginCount < 0){
        $('#mainnav').addClass('bg');
        $('#mainnav').css('margin-top', '0px');
      }else if(ScrollTop < nextSection && marginCount < 60 ){         
         $('#mainnav').css({
          'margin-top' : marginCount
        });
      }else if(ScrollTop < ourSection && marginCount < 60 ){         
         $('#mainnav').css({
          'margin-top' : ourSection - ScrollTop
        });
      }else{
        $('#mainnav').css('margin-top', '15px');
        $('#mainnav').removeClass('bg');
        if(windowWidth < 768){
          $('#mainnav').css('margin-top', '15px');
        }
      }
    });

   var sections = $('section')
      , nav = $('.nav.navbar-nav')
      , nav_height = nav.outerHeight()
      , windowWidth = $(window).width();

    $(window).on('scroll', function () {
      var cur_pos = $(this).scrollTop();
      
      sections.each(function() {
        if($(window).width() < 768){
          var top = $(this).offset().top - (nav_height + 62);
          var bottom = top + $(this).outerHeight();
          if (cur_pos >= top && cur_pos <= bottom) {
            nav.find('a').removeClass('active');
            sections.removeClass('active');
            
            $(this).addClass('active');
            nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
          }
        }else{
          var top = $(this).offset().top - (nav_height + 60);
          var bottom = top + $(this).outerHeight();
          if (cur_pos >= top && cur_pos <= bottom) {
            nav.find('a').removeClass('active');
            sections.removeClass('active');
            
            $(this).addClass('active');
            nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
          }
        }        
      });
    });

    $('.totop').click(function(event){
      var target = $( $(this).attr('href') );

      if( target.length ) {
          event.preventDefault();
          $('html, body').animate({
              scrollTop: target.offset().top - 60
          }, 600);
      }
    });
  });

</script>
<?php
  }
?>
<?php include('layouts/includes/ourservice.php'); ?>
<?php include('layouts/includes/howtowork.php'); ?>
<?php include('layouts/includes/portfolio.php'); ?>
<?php include('layouts/includes/ourteam.php'); ?>
<?php include('layouts/includes/contact.php'); ?>

<?php get_footer(); ?>
